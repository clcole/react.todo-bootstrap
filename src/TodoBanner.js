import React from 'react';

export function TodoBanner({ name, items }) {
  return (
    <h4 className="bg-primary text-white text-center p-2">
      {name} - To Do List
      ({items.filter(item => !item.done).length} items to do)
    </h4>);
}
