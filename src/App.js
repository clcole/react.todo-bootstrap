import React, { Component } from 'react';
import { TodoBanner } from './TodoBanner';
import { TodoRow } from './TodoRow';
import { TodoCreator } from './TodoCreator';
import { VisibilityControl } from "./VisibilityControl";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "Ray",
      todoItems: [
        { action: "eat", done: false },
        { action: "nap", done: false },
        { action: "read", done: false },
        { action: "exercise", done: false },
      ],
      showDone: false
    };
  }

  createNewTodo = (newItemText) => {
    if(!this.state.todoItems.find(item => item.action === newItemText)) {
      this.setState({ 
        todoItems: [
          ...this.state.todoItems, 
          { action: newItemText, done: false }]
      }, () => localStorage.setItem("rayTodos", JSON.stringify(this.state)));
    }
  };

  toggleTodo = (todo) => this.setState({ 
    todoItems: this.state.todoItems.map(
      item => item.action === todo.action ? { action: item.action, done: !item.done } : item)
  }, () => localStorage.setItem("rayTodos", JSON.stringify(this.state)));

  todoRows = (doneValue) => this.state.todoItems.filter(item => 
    item.done === doneValue).map(item => 
      <TodoRow key={item.action} item={item} callback={this.toggleTodo} />);

  componentDidMount = () => {
    const data = localStorage.getItem("rayTodos");
    
    if(data !== null) {
      this.setState(JSON.parse(data));
    }

    // this.setState(data != null
    //   ? JSON.parse(data)
    //   : {
    //     userName: "Ray",
    //     todoItems: [
    //       { action: "eat", done: false },
    //       { action: "nap", done: false },
    //       { action: "read", done: true },
    //       { action: "exercise", done: false },
    //     ],
    //     showDone: true
    //   });
  };

  componentWillUnmount = () => { 
    localStorage.removeItem("rayTodos"); 
  };

  render() {
    return (  
      <div>
        <TodoBanner name={this.state.userName} items={this.state.todoItems} />
        <div className="container-fluid">
          <TodoCreator callback={this.createNewTodo} />
          <table className="table table-striped table-bordered">
            <thead><tr><th>Description</th><th>Done</th></tr></thead>
            <tbody>{this.todoRows(false)}</tbody>
          </table>
          <div className="bg-secondary text-white text-center p-2">
            <VisibilityControl
              description="Completed Items"
              isChecked={this.state.showDone}
              callback={(checked) => this.setState({ showDone: checked },
                () => localStorage.setItem("rayTodos", JSON.stringify(this.state)))} />
          </div>
          {this.state.showDone &&
            <table className="table table-striped table-bordered">
              <thead><tr><th>Description</th><th>Done</th></tr></thead>
              <tbody>{this.todoRows(true)}</tbody>
            </table>
          }
        </div>
      </div >
    );
  }
}

export default App;
